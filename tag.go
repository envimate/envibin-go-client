package client

import (
	"bitbucket.org/envimate/envibin-cli/domain"
	"fmt"
	"strings"
	"net/http"
)

//Tag adds a tag to an existing version by the given artifact key.
func (c *Client) Tag(artifactKey *domain.ArtifactKey, tags []string) error {
	endpoint := fmt.Sprintf("/%s/versions/%s/tags", artifactKey.Name, artifactKey.Version)

	resp, err := c.request().
		SetQueryParam("tag", strings.Join(tags, ",")).
		Put(endpoint)

	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}

//RemoveTag adds a tag to an existing version by the given artifact key.
func (c *Client) RemoveTag(artifactKey *domain.ArtifactKey, tags []string) error {
	endpoint := fmt.Sprintf("/%s/versions/%s/tags", artifactKey.Name, artifactKey.Version)

	resp, err := c.request().
		SetQueryParam("tag", strings.Join(tags, ",")).
		Delete(endpoint)

	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}