package client

import (
	"bitbucket.org/envimate/envibin-cli/domain"
	"fmt"
	"strings"
	"net/http"
)

//Label adds a label to an existing version by the given artifact key.
func (c *Client) Label(artifactKey *domain.ArtifactKey, labels []string) error {
	endpoint := fmt.Sprintf("/%s/versions/%s/labels", artifactKey.Name, artifactKey.Version)

	resp, err := c.request().
		SetQueryParam("label", strings.Join(labels, ",")).
		Put(endpoint)

	if err != nil {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), err)
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}

//RemoveLabel removes a label of an existing version by the given artifact key.
func (c *Client) RemoveLabel(artifactKey *domain.ArtifactKey, labels []string) error {
	endpoint := fmt.Sprintf("/%s/versions/%s/labels", artifactKey.Name, artifactKey.Version)

	resp, err := c.request().
		SetQueryParam("label", strings.Join(labels, ",")).
		Delete(endpoint)

	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}