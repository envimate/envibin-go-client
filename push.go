package client

import (
	"fmt"
	"strings"
	"bitbucket.org/envimate/envibin-cli/domain"
	"net/http"
	"path/filepath"
)

//Push creates or puts a version with the given artifact name and version number.
//The file at the given filepath will be uploaded as the version's binary.
//Given labels and tags will be set on the new version.
func (c *Client) Push(artifactKey *domain.ArtifactKey, filePath string, tags, labels []string) error {
	endpoint := fmt.Sprintf("/%s/versions/%s", artifactKey.Name, artifactKey.Version)

	resp, err := c.request().
		SetFile("file", filePath).
		SetQueryParam("tag", strings.Join(tags, ",")).
		SetQueryParam("label", strings.Join(labels, ",")).
		SetHeader("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, filename(filePath))).
		Put(endpoint)

	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}

func filename(filePath string) string {
	_, file := filepath.Split(filePath)
	return file
}