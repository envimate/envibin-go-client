package client

import (
	"fmt"
	"strings"
	"net/http"
	"time"
	"sort"
	"errors"
)

//GetVersions returns all versions for the given artifact name.
//Output will be filtered based on given tags and labels.
//Returns an empty array when the artifact has no versions or does not exist.
func (c *Client) GetVersions(artifactName string, tags []string, labels []string) ([]Version, error) {
	endpoint := fmt.Sprintf("/%s/versions/", artifactName)
	resp, err := c.request().
		SetQueryParam("tag", strings.Join(tags, ",")).
		SetQueryParam("label", strings.Join(labels, ",")).
		SetResult([]Version{}).
		Get(endpoint)
	if err != nil {
			return []Version{}, err
	}
	if resp.StatusCode() != http.StatusOK {
		return []Version{}, errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	versions := resp.Result().(*[]Version)
	sort.Sort(byLastFirst(*versions))

	if len(*versions) == 0 {
		return []Version{}, errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), errors.New("no versions found"))
	}

	return *versions, nil
}

//GetLatestPerArtifact returns a latest version for each available artifact.
//Returns an empty array when the configured Envibin repository is empty.
func (c *Client) GetLatestPerArtifact() ([]Version, error) {
	endpoint := "/"
	resp, err := c.request().
		SetResult([]Version{}).
		Get(endpoint)
	if err != nil {
		return []Version{}, err
	}
	if resp.StatusCode() != http.StatusOK {
		return []Version{}, errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	versions := resp.Result().(*[]Version)
	sort.Sort(byLastFirst(*versions))

	if len(*versions) == 0 {
		return []Version{}, errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), errors.New("no versions found"))
	}

	return *versions, nil
}

//Version contains version data.
type Version struct {
	ArtifactName                  string   `json:"artifactName"`
	VersionNumber                 string   `json:"versionNumber"`
	Labels                        []string `json:"labels"`
	Tags                          []string `json:"tags"`
	VersionTimestampInEpochMillis int64    `json:"versionTimestampEpochMilliSeconds"`
}

func (v *Version) Timestamp() string {
	timestamp := time.Unix(0, v.VersionTimestampInEpochMillis * int64(time.Millisecond))
	return fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d",
		timestamp.Year(),
		timestamp.Month(),
		timestamp.Day(),
		timestamp.Hour(),
		timestamp.Minute(),
		timestamp.Second())
}

type byLastFirst []Version

func (a byLastFirst) Len() int           { return len(a) }
func (a byLastFirst) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a byLastFirst) Less(i, j int) bool {
	return a[i].VersionTimestampInEpochMillis > a[j].VersionTimestampInEpochMillis
}
