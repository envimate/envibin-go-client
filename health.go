package client

const statusDown = "DOWN"
const statusUp = "UP"

// GetHealth returns version and build information of the configured repository.
func (c *Client) GetHealth() (*Health, error) {
	endpoint := "/health"
	resp, err := c.request().
		SetResult(Health{}).
		Get(endpoint)
	if err != nil {
		return nil, errorF(resp.Request.Method, resp.Request.URL, resp.StatusCode(), err)
	}

	health := resp.Result().(*Health)
	if resp.StatusCode() == 200 {
		health.Status = statusUp
	} else {
		health.Status = statusDown
	}


	return health, nil
}

// Health contains version and build information of a Envibin repository.
type Health struct {
	Version string `json:"version"`
	Status string `json:"status"`
}