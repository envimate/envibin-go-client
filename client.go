package client

import (
	"github.com/go-resty/resty"
	"encoding/json"
	"fmt"
)

// Client contains Envibin repository information and functionality.
type Client struct {
	url string
}

// New creates a Client.
func New(url string) (*Client, error) {
	return &Client{url}, nil
}

func (c *Client) request() *resty.Request {
	restyClient := resty.New()
	restyClient.SetHostURL(c.url)

	return restyClient.R()
}

func errorF(method, url string, status int, err error) error {
	return fmt.Errorf("%s %s %d - %s", method, url, status, err.Error())
}

func extractErrorMessage(body []byte) error {
	var errResp errorResponse
	err := json.Unmarshal(body, &errResp)
	if err != nil {
		return fmt.Errorf("unexpected server response")
	}

	return fmt.Errorf("%s: %s", errResp.Error, errResp.Msg)
}

type errorResponse struct {
	Error string `json:"error"`
	Msg string `json:"message"`
}