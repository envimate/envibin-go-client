package client

import (
	"bitbucket.org/envimate/envibin-cli/domain"
	"fmt"
	"os"
	"io"
	"net/http"
	"mime"
	"errors"
)

//PullByVersion download the binaries of a version by the given artifact key (name:version/tag).
//A file will be create a file containing the artifact name and version number.
//By default the file will be created under the current directory.
//The output directory can be changed by giving a location string.
func (c *Client) Pull(key *domain.ArtifactKey, location string) error {
	var endpoint string

	endpoint = fmt.Sprintf("/%s/%s", key.Name, key.Version)

	return c.abstractPull(endpoint, key, location)
}

//PullByVersion download the binaries of a version by the given artifact key (name:version).
//A file will be create a file containing the artifact name and version number.
//By default the file will be created under the current directory.
//The output directory can be changed by giving a location string.
func (c *Client) PullByVersion(key *domain.ArtifactKey, location string) error {
	var endpoint string

	endpoint = fmt.Sprintf("/%s/versions/%s", key.Name, key.Version)

	return c.abstractPull(endpoint, key, location)
}

//PullByTag download the binaries of a version by the given artifact key (name:tag).
//A file will be create a file containing the artifact name and version number.
//By default the file will be created under the current directory.
//The output directory can be changed by giving a location string.
func (c *Client) PullByTag(key *domain.ArtifactKey, location string) error {
	var endpoint string

	endpoint = fmt.Sprintf("/%s/tags/%s", key.Name, key.Version)

	return c.abstractPull(endpoint, key, location)
}

func (c *Client) abstractPull(endpoint string, key *domain.ArtifactKey, location string) error {
	resp, err := c.request().
		SetDoNotParseResponse(true).
		Get(endpoint)
	if err != nil {
		return err
	}
	defer resp.RawBody().Close()

	code := resp.StatusCode()
	if code >= 400 && code <= 499 {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), errors.New("no artifact found"))
	} else if code >= 500 {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), errors.New("internal server errorF"))
	} else if code != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	filename := resolveFilename(
		key,
		location,
		resp.Header().Get("Content-Disposition"))
	out, err := os.Create(filename)
	if err != nil {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	_, err = io.Copy(out, resp.RawBody())
	if err != nil {
		os.Remove(filename)
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}

func resolveFilename(key *domain.ArtifactKey, location string, contentDisposition string) string {
	if location != "" {
		return location
	} else if originalFilename := filenameFromDisposition(contentDisposition); originalFilename != "" {
		return originalFilename
	} else {
		return fmt.Sprintf("./%s-%s", key.Name, key.Version)
	}
}

func filenameFromDisposition(contentDisposition string) string {
	_, params, err := mime.ParseMediaType(contentDisposition)
	if err != nil {
		return ""
	}
	return params["filename"]
}