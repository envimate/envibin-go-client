package client

import (
	"bitbucket.org/envimate/envibin-cli/domain"
	"net/http"
	"fmt"
)

//Remove removes an artifact or artifact version depening on the given artifact key.
//If no artifact version is provided in the artifact key, the artifact will be removed.
//If an artifact version is provided in the artifact key, the version will be removed.
func (c *Client) Remove(artifactKey *domain.ArtifactKey) error {
	var endpoint string
	if artifactKey.Name == "" {
		return fmt.Errorf("artifact name cannot be empty")
	} else if artifactKey.Version == "" {
		endpoint = fmt.Sprintf("/%s/", artifactKey.Name)
	} else {
		endpoint = fmt.Sprintf("/%s/versions/%s/", artifactKey.Name, artifactKey.Version)
	}

	resp, err := c.request().
		Delete(endpoint)

	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return nil
}
