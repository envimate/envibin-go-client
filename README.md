# Envibin Go Client

This package contains a client to communicatie with Envimate's Envibin. Read godocs for more information on available functions.

### Usage
To gain access to the API call simply create a new client.
```
package main

import "bitbucket.org/envimate/envibin-go-client"

func main() {
    c, err := client.New("http://localhost:8080")
	if err != nil {
		error(err)
	}

    // List versions
    versions, err = c.GetVersions("com.envimate.ubuntu-xenial", []string{}, []string{})
	if err != nil {
		error(err)
	}
	fmt.Println(versions)
}
```
