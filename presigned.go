package client

import (
	"bitbucket.org/envimate/envibin-cli/domain"
	"fmt"
	"net/http"
)

func (c *Client) GetPresignedUrl(key *domain.ArtifactKey) (string, error) {
	endpoint := fmt.Sprintf("/%s/presigned/%s", key.Name, key.Version)
	return c.abstractGetPresignedUrl(endpoint, key)
}

func (c *Client) GetPresignedUrlByTag(key *domain.ArtifactKey) (string, error) {
	endpoint := fmt.Sprintf("/%s/presigned/tags/%s", key.Name, key.Version)
	return c.abstractGetPresignedUrl(endpoint, key)
}

func (c *Client) GetPresignedUrlByVersion(key *domain.ArtifactKey) (string, error) {
	endpoint := fmt.Sprintf("/%s/presigned/versions/%s", key.Name, key.Version)
	return c.abstractGetPresignedUrl(endpoint, key)
}

func (c *Client) abstractGetPresignedUrl(endpoint string, key *domain.ArtifactKey) (string, error) {
	resp, err := c.request().
		Get(endpoint)
	if err != nil {
		return "", err
	}
	if resp.StatusCode() != http.StatusOK {
		return "", errorF(resp.Request.Method, resp.Request.URL,resp.StatusCode(), extractErrorMessage(resp.Body()))
	}

	return string(resp.Body()), nil
}